SEE ALSO
--------
man:megatools[1],
man:megarc[5],
man:megatools-df[1],
man:megatools-dl[1],
man:megatools-get[1],
man:megatools-ls[1],
man:megatools-mkdir[1],
man:megatools-put[1],
man:megatools-reg[1],
man:megatools-rm[1],
man:megatools-copy[1].


MEGATOOLS
---------
Part of the man:megatools[1] suite of commands.


BUGS
----

There is no upstream support for bugreports and feature requests. But you 
can send code patches to megatools@xff.cz to get them integrated into
upstream repository.


AUTHOR
------

Megatools was written by Ondrej Jirman <megatools@xff.cz>, 2013-2022.

Official website is link:https://xff.cz/megatools/[].
